import filecmp
import unittest
from pathlib import Path
from hockeyjockey.models import Team, Game
from hockeyjockey.utilities.file import serialize, deserialize


class TestSerial(unittest.TestCase):
    """
    Class for testing serialization/deserialization.
    """

    def setUp(self) -> None:
        """
        Runs once before each test. Sets up the file paths needed for testing
        serialization/deserialization.

        :return: None.
        """

        self.teams_master_path_ser = Path(__file__).parent / 'test_data/serialized/master.teams.data'
        self.teams_target_path_ser = Path(__file__).parent / 'test_data/serialized/teams.data'
        self.teams_target_path_de_ser = Path(__file__).parent / 'test_data/serialized/teams.data'
        self.teams = []

        self.games_master_path_ser = Path(__file__).parent / 'test_data/serialized/master.games.data'
        self.games_target_path_ser = Path(__file__).parent / 'test_data/serialized/games.data'
        self.games_target_path_de_ser = Path(__file__).parent / 'test_data/serialized/games.data'
        self.games = []

        # Setup up a list of team objects to serialize
        for i in range(1, 5):
            tm = Team(i, 'TestTeam' + str(i), 'TT' + chr(i))
            self.teams.append(tm)

        for i in range(1, 4):
            gm = Game(home_team=self.teams[i], away_team=self.teams[i], date='2020-01-01', winning_team=-1)
            self.games.append(gm)

    def test_serialize_teams(self) -> None:
        """
        Tests the serialize method and compares the result to the expected result contained on
        a file on disk.

        :return: None.
        """

        serialize(self.teams, self.teams_target_path_ser)
        # Compare the serialized file to a master copy stored earlier
        self.assertTrue(filecmp.cmp(self.teams_target_path_ser, self.teams_master_path_ser), msg='[{}] IS NOT '
                                                                                                 'identical to [{'
                                                                                                 '}]'.format(
            self.teams_target_path_ser, self.teams_master_path_ser))

    def test_serialize_games(self):
        """
        Tests the serialize method and compares the result to the expected result contained on
        a file on disk.

        :return: None.
        """

        serialize(self.games, self.games_target_path_ser)
        # Compare the serialized file to a master copy stored earlier
        self.assertTrue(filecmp.cmp(self.games_target_path_ser, self.games_master_path_ser), msg='[{}] IS NOT '
                                                                                                 'identical to [{'
                                                                                                 '}]'.format(
            self.games_target_path_ser, self.games_master_path_ser))

    def test_deserialize_teams(self):
        """
        Tests the deserialize method and compares the result to an expected object
        generated in the setUp method.

        :return: None.
        """

        file_to_deserialize = self.teams_target_path_de_ser

        resulting_obj = deserialize(file_to_deserialize)

        self.assertListEqual(resulting_obj, self.teams)

    def test_deserialize_games(self):
        """
        Tests the deserialize method and compares the result to an expected object
        generated in the setUp method.

        :return: None.
        """

        file_to_deserialize = self.games_target_path_de_ser
        resulting_obj = deserialize(file_to_deserialize)
        self.assertListEqual(resulting_obj, self.games)


if __name__ == '__main__':
    unittest.main()

