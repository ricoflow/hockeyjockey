import unittest
import mysql.connector
from unittest import TestCase, mock
from hockeyjockey.config import db as db_cfg
from hockeyjockey.automation import capture_winners, capture_predictions
from hockeyjockey.utilities.db import hj_connect, connect, hj_connect_test
from mysql.connector.connection_cext import CMySQLConnection as mysql_connection_obj

# using the hockey_jockey test database
TEST_DB = db_cfg.db_name_test


class TestDB(TestCase):
    """
    Class for testing database connections and queries.
    """

    @classmethod
    def setUpClass(cls) -> None:
        """
        Sets up the test database and creates some tables for testing.

        :return: None
        """
        # runs once before each test
        # connect as a user that has DROP/CREATE permission for TEST_hockey_jockey database
        cnx = mysql.connector.connect(
            host=db_cfg.db_host,
            user=db_cfg.user_rw,
            password=db_cfg.pass_rw,
            port=db_cfg.db_port
        )
        cursor = cnx.cursor()

        # DROP DATABASE if it already exists
        try:
            cursor.execute(f'DROP DATABASE {TEST_DB}')
            print(f'{TEST_DB} database dropped.')
        except mysql.connector.Error as err:
            print(f'{TEST_DB} - {err}')

        # CREATE the test database
        try:
            cursor.execute(f'CREATE DATABASE IF NOT EXISTS {TEST_DB}')
            print(f'{TEST_DB} created.')
            cursor.execute(f'USE {TEST_DB}')
        except mysql.connector.Error as err:
            print(f'{TEST_DB} - {err}')

        # CREATE the tables
        stats_def_create_query = """CREATE TABLE `stat_def` (
                                  `id` int(10) NOT NULL AUTO_INCREMENT,
                                  `statsapi_name` varchar(45) NOT NULL,
                                  `readable_name` varchar(45) NOT NULL,
                                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;"""

        teams_def_create_query = """CREATE TABLE `teams_def` (
                                  `id` int(11) NOT NULL,
                                  `abbrev` varchar(5) NOT NULL,
                                  `name` varchar(30) NOT NULL,
                                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"""

        games_create_query = """CREATE TABLE `games` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `a_id` int(11) NOT NULL,
                                  `h_id` int(11) NOT NULL,
                                  `gm_dt` datetime NOT NULL,
                                  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                  PRIMARY KEY (`id`),
                                  KEY `fk_away_team_idx` (`a_id`),
                                  KEY `fk_home_team_idx` (`h_id`),
                                  CONSTRAINT `fk_away_team` FOREIGN KEY (`a_id`) REFERENCES `teams_def` (`id`),
                                  CONSTRAINT `fk_home_team` FOREIGN KEY (`h_id`) REFERENCES `teams_def` (`id`)
                                ) ENGINE=InnoDB AUTO_INCREMENT=932 DEFAULT CHARSET=utf8;"""

        daily_ranks_create_query = """CREATE TABLE `daily_ranks` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `ranking` int(11) NOT NULL,
                                      `game_id` int(11) NOT NULL,
                                      `stat_def_id` int(11) NOT NULL,
                                      `pred_winner_id` int(11) NOT NULL,
                                      `actual_winner_id` int(11) NOT NULL,
                                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      PRIMARY KEY (`id`),
                                      KEY `fk_stat_idx` (`stat_def_id`),
                                      KEY `fk_games_idx` (`game_id`),
                                      KEY `fk_winning_team_idx` (`actual_winner_id`),
                                      KEY `fk_pred_winning_team_idx` (`pred_winner_id`),
                                      CONSTRAINT `fk_games` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
                                      CONSTRAINT `fk_pred_winning_team` FOREIGN KEY (`pred_winner_id`) REFERENCES `teams_def` (`id`),
                                      CONSTRAINT `fk_stat` FOREIGN KEY (`stat_def_id`) REFERENCES `stat_def` (`id`),
                                      CONSTRAINT `fk_winning_team` FOREIGN KEY (`actual_winner_id`) REFERENCES `teams_def` (`id`)
                                    ) ENGINE=InnoDB AUTO_INCREMENT=27000 DEFAULT CHARSET=utf8;"""

        daily_stats_create_query = """CREATE TABLE `daily_stats` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `team_id` int(11) NOT NULL,
                                      `stat_def_id` int(11) NOT NULL,
                                      `value` float NOT NULL,
                                      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      PRIMARY KEY (`id`),
                                      KEY `fk_team_idx` (`team_id`),
                                      KEY `fk_stat_def_idx` (`stat_def_id`),
                                      CONSTRAINT `fk_stat_def` FOREIGN KEY (`stat_def_id`) REFERENCES `stat_def` (`id`),
                                      CONSTRAINT `fk_team` FOREIGN KEY (`team_id`) REFERENCES `teams_def` (`id`)
                                    ) ENGINE=InnoDB AUTO_INCREMENT=142043 DEFAULT CHARSET=utf8;"""

        try:
            cursor.execute(stats_def_create_query)
            cursor.execute(teams_def_create_query)
            cursor.execute(games_create_query)
            cursor.execute(daily_ranks_create_query)
            cursor.execute(daily_stats_create_query)
            # commit the changes
            cnx.commit()
            # close the connection
            cursor.close()
            cnx.close()
        except mysql.connector.Error as err:
            print(f'{TEST_DB} - {err}')

    def setUp(self) -> None:
        """
        Runs before each test. Establishes a connection to the test database.

        :return: None.
        """
        self.test_cnx = mysql.connector.connect(
            host=db_cfg.db_host,
            user=db_cfg.user_rw,
            password=db_cfg.pass_rw,
            # important - test database used here:
            database=db_cfg.db_name_test,
            port=db_cfg.db_port
        )

    @classmethod
    def tearDownClass(cls) -> None:
        """
        Runs once after all tests. Drops the test database.

        :return: None.
        """
        cnx = mysql.connector.connect(
            host=db_cfg.db_host,
            user=db_cfg.user_rw,
            password=db_cfg.pass_rw,
            port=db_cfg.db_port
        )
        cursor = cnx.cursor()

        # DROP DATABASE if it already exists
        try:
            cursor.execute(f'DROP DATABASE {TEST_DB}')
            print(f'{TEST_DB} database dropped.')
        except mysql.connector.Error as err:
            print(f'{TEST_DB} - {err}')

        # commit the changes
        cnx.commit()
        # close the connection
        cursor.close()
        cnx.close()

    # utilities.db
    def test_connect(self) -> None:
        """
        Tests the generic database connection method.

        :return: None
        """
        self.assertIsInstance(connect(db_cfg.db_host, db_cfg.user_rw, db_cfg.db_name, db_cfg.pass_rw, db_cfg.db_port),
                              mysql_connection_obj)

    def test_hj_connect(self) -> None:
        """
        Tests the hockeyjockey production database connection.

        :return: None.
        """
        self.assertIsInstance(hj_connect(),
                              mysql_connection_obj)

    def test_hj_connect_testdb(self) -> None:
        """
        Tests the hockeyjockey TEST database connection.

        :return: None.
        """
        self.assertIsInstance(hj_connect_test(),
                              mysql_connection_obj)


    # TODO: test fails unless exception is raised. Function needs to be re-written
#    def test_capture_winners_test(self) -> None:
#        """
#         Mocks the production database connection with the TEST database
#         connection and attempts to fill the tables with 'winner' data.
#
#        :return: None.
#        """
#        with mock.patch('hockeyjockey.utilities.db') as hj_connect_mock:
#            hj_connect_mock.hj_connect.return_value = self.test_cnx
#            with self.assertRaises(SystemExit) as cm:
#                capture_winners()
#            self.assertEqual(cm.exception.code, 0)

    #TODO: test fails unless exception is raised. Function needs to be re-written
#    def test_capture_predictions_test(self):
#        """
#         Mocks the production database connection with the TEST database
#         connection and attempts to fill the tables with 'prediction' data.
#
#        :return: None.
#        """
#        with mock.patch('hockeyjockey.utilities.db') as hj_connect_mock:
#            hj_connect_mock.hj_connect.return_value = self.test_cnx
#            with self.assertRaises(SystemExit) as cm:
#                capture_predictions()
#            self.assertEqual(cm.exception.code, 0)

    # TODO - add and fix the rest of the functions from capture.py module


if __name__ == '__main__':
    unittest.main()
