import os
import unittest
from pathlib import Path
from unittest import TestCase
from freezegun import freeze_time
from hockeyjockey.utilities.stats import floatize, calc_pdo
from hockeyjockey.utilities.file import get_hj_dir, get_hj_file_path
from hockeyjockey.utilities.date import valid_date, yesterday, today, closest_fri_sat


class TestUtilities(TestCase):
    """
    Class for testing some of the utility methods.
    """

    def setUp(self) -> None:
        """
        Sets up file paths for testing file/directory creation methods.

        :return: None.
        """

        # TODO: These paths might be problematic for the GitLab CI/CD integrations
        self.test_data_dir_loc = Path(__file__).parent
        self.test_data_dir_name = 'test_data'
        self.test_filename = 'test.data'
        self.test_hj_dir = os.path.join(os.path.expanduser(self.test_data_dir_loc), self.test_data_dir_name)
        self.full_test_path = os.path.join(self.test_hj_dir, self.test_filename)

    # utilities.date
    def test_valid_date(self) -> None:
        """
        Tests the valid_date method with different inputs.

        :return: None.
        """

        self.assertEqual(valid_date('2020-01-01'), '2020-01-01')
        self.assertEqual(valid_date(1), None)
        self.assertEqual(valid_date('random_string'), None)
        self.assertEqual(valid_date('01/01/2020'), None)

    @freeze_time('2020-01-01')
    def test_today(self) -> None:
        """
        Tests the today method. Uses freeze_time decorator to mock the current date.

        :return: None.
        """
        self.assertEqual(today(), '2020-01-01')

    @freeze_time('2020-01-01')
    def test_yesterday(self) -> None:
        """
        Tests the yesterday method. Uses freeze_time decorator to mock the current date.

        :return: None.
        """
        self.assertEqual(yesterday(), '2019-12-31')

    # Current day is January 1st
    @freeze_time('2020-01-01')
    def test_closest_fri_sat_onjan1(self) -> None:
        """
        Tests the closest_fri_sat method. Uses freeze_time decorator to mock the current date.
        Edge case of January 1st.

        :return: None.
        """

        self.assertEqual(closest_fri_sat(), ('2020-01-03', '2020-01-04'))

    # Current date is a Friday
    @freeze_time('2019-12-27')
    def test_closest_fri_sat_onfri(self) -> None:
        """
        Tests the closest_fri_sat method. Uses freeze_time decorator to mock the current date.
        Edge case of 'current date is a Friday'.

        :return: None.
        """
        self.assertEqual(closest_fri_sat(), ('2019-12-27', '2019-12-28'))

    # Current date is a Saturday
    @freeze_time('2019-12-28')
    def test_closest_fri_sat_onsat(self) -> None:
        """
        Tests the closest_fri_sat method. Uses freeze_time decorator to mock the current date.
        Edge case of 'current date is a Saturday'.

        :return: None.
        """
        self.assertEqual(closest_fri_sat(), ('2019-12-27', '2019-12-28'))

    # utilities.file
    # TODO : Test this by passing invalid paths
    def test_get_hj_dir(self) -> None:
        """
        Tests the get_hj_dir method. Asserts the directory exists.

        :return: None.
        """
        self.assertTrue(os.path.exists(get_hj_dir()))

    # TODO : Test this by passing invalid paths
    def test_get_hj_file_path(self) -> None:
        """
        Tests the get_hj_file_path mehtod. Asserts the path is equal to the expected path.

        :return: None.
        """
        path = get_hj_file_path(self.test_hj_dir, self.test_filename)
        self.assertEqual(get_hj_file_path(self.test_hj_dir, self.test_filename), self.full_test_path)

    # utilities.stats
    def test_floatize(self) -> None:
        """
        Tests the floatize method using several inputs of float, string, int
        including negative and positive values.

        :return: None.
        """
        # random string
        self.assertEqual(floatize('hello world!'), 0.0)

        # string to float
        self.assertEqual(floatize('5.1'), 5.1)

        # negative string to float
        self.assertEqual(floatize('-5.1'), -5.1)

        # positive string to float
        self.assertEqual(floatize('+4.5'), 4.5)

        # multiple values in string to float - program should take the first occurrence
        self.assertEqual(floatize('There are -4.5 and +3 and -0.5 and 0 in this string'), -4.5)

        # int to float
        self.assertEqual(floatize(5), 5.0)

        # zero value int
        self.assertEqual(floatize(0), 0.0)

        # zero value float
        self.assertEqual(floatize(0.0), 0.0)

        # negative int to float
        self.assertEqual(floatize(-1), -1.0)

        # float to float
        self.assertEqual(floatize(5.01), 5.01)

        # negative float to float
        self.assertEqual(floatize(-5.01), -5.01)

    def test_calc_pdo(self) -> None:
        """
        Tests the calc_pdo function.

        :return: None.
        """

        self.assertEqual(calc_pdo(0.87, 0.87), 878.7)
        # TODO: add more assertions here for string, int, etc...


if __name__ == '__main__':
    unittest.main()
