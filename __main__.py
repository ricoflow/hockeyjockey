from hockeyjockey import Jockey


def main():
    jockey = Jockey(suppress_prompt=True)
    jockey.menu_main.display_menu()
    print('done')


if __name__ == '__main__':
    main()
